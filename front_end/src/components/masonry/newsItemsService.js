'use strict';

angular.module('newsItemsService', [])
  .factory('NewsItems', function($resource){
      return $resource('../components/masonry/output.json', {}, {
      });
    });