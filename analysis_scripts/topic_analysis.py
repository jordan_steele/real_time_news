from gensim import corpora, models, similarities

def create_topic_model(coll, ntopics):
    dict_ = corpora.Dictionary([ doc.get_terms_list() for doc in coll ])
    corpus = [ dict_.doc2bow(doc.get_terms_list()) for doc in coll ]
    tfidf = models.TfidfModel(corpus)
    corpus_tfidf = tfidf[corpus]

    lsi = models.LsiModel(corpus_tfidf, id2word=dict_, num_topics=ntopics)
    corpus_lsi = lsi[corpus_tfidf]

    index = similarities.MatrixSimilarity(corpus_lsi)

    return (dict_, lsi, corpus_lsi, index)

def qry_topic_model(doc, dict_, lsi, sim_index):
    qry_bow = dict_.doc2bow(doc.get_terms_list())
    qry_lsi = lsi[qry_bow]
    res = list(enumerate(sim_index[qry_lsi]))
    res = sorted(res, key=lambda item: -item[1])

    return res