import praw
import coll
from newspaper import Article

def create_reddit_conn():
    api = praw.Reddit('jordansteeleissick')
    api.config.store_json_result = True
    return api

def parse_sub(sub):
    return ([sub.title] * 10) + [ comment.body for comment in sub.comments
                                    if isinstance(comment, praw.objects.Comment) ] 

def get_image_url(sub):
    article = Article(sub.url, 'en')
    article.download()
    article.parse()
    return article.top_image

def parse_subs(subs):
    ignore_domains = "i.imgur.com"
    parsed_subs = []
    for sub in subs:
        if not sub.media and sub.domain not in ignore_domains:
            parsed_subs += [(sub, parse_sub(sub), get_image_url(sub))]

    return coll.build_coll(parsed_subs, coll.RedditSub)

if __name__ == '__main__':
    api = create_reddit_conn()
    print parse_subs(api.get_top(limit=25))
