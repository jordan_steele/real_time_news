import math
import re
import stop_words as sw
import get_twitter_trends as gtt

class BowDoc:
    """Bag-of-words representation of a document.

    The document has an ID, and an iterable list of terms with their
    frequencies."""

    def __init__(self, docid):
        """Constructor.

        Set the ID of the document, and initiate an empty term dictionary.
        Call add_term to add terms to the dictionary."""
        self.docid = docid
        self.terms_list = []
        self.terms_dict = {}

    def add_terms(self, terms_):
        """Add a term occurrence to the BOW representation.

        This should be called each time the term occurs in the document."""
        self.terms_list += terms_
        for term in terms_:
            try:
                self.terms_dict[term] += 1
            except KeyError:  
                self.terms_dict[term] = 1

    def get_term_count(self, term):
        """Get the term occurrence count for a term.

        Returns 0 if the term does not appear in the document."""
        try:
            return self.terms_dict[term]
        except KeyError:
            return 0

    def get_term_freq_dict(self):
        """Return dictionary of term:freq pairs."""
        return self.terms_dict

    def get_terms_list(self):
        return self.terms_list

    def get_term_list(self):
        """Get sorted list of all terms occurring in the document."""
        return sorted(self.terms_dict.keys())

    def get_docid(self):
        """Get the ID of the document."""
        return self.docid

    def get_tfidf_scores(self, doc_freqs, ndocs):
        """Calculates the tf*idf score for each term in the document.

        Returns a dictionary from terms to tf*idf scores."""
        tfidf_scores = {}
        for (term, freq) in self:
            tfidf_scores[term] = math.log(freq + 1) * math.log(float(ndocs) / doc_freqs[term])
        return tfidf_scores

    def set_docid(self, docid):
        self.docid = docid

    def __iter__(self):
        """Return an ordered iterator over term--frequency pairs.

        Each element is a (term, frequency) tuple.  They are iterated
        in alphabetical order."""
        return iter(sorted(self.terms_dict.iteritems()))

class RTNBowDoc(BowDoc):

    def __init__(self, docid, base_obj, image_url):
        BowDoc.__init__(self, docid)
        self.base_obj = base_obj
        self.image_url = image_url

    def get_base_obj(self):
        return self.base_obj

    def get_image_url(self):
        # return self.image_url
        if self.image_url:
            return self.image_url
        else:
            return "http://lorempixel.com/g/280/295/?4476"

class TwitterTrend(RTNBowDoc):

    def __init__(self, docid, trend_obj, image_url):
        RTNBowDoc.__init__(self, docid, trend_obj, image_url)
        # self.trend_obj = trend_obj

    def get_title(self):
        return gtt.remove_hash_split_words(self.base_obj['name'])

    def get_created(self):
        return self.base_obj.created_at

    def get_json(self):
        return self.base_obj

class RedditSub(RTNBowDoc):

    def __init__(self, docid, sub_obj, image_url):
        RTNBowDoc.__init__(self, docid, sub_obj, image_url)
        # self.sub_obj = sub_obj

        # article = Article(sub_obj.url, 'en')
        # article.download()
        # article.parse()
        # self.image_url = article.top_image

    def get_title(self):
        return self.base_obj.title

    def get_created(self):
        return self.base_obj.created_utc

    def get_json(self):
        return self.base_obj.json_dict

class BowColl:
    """Collection of BOW documents."""

    def __init__(self):
        """Constructor.

        Creates an empty collection."""
        self.docs = {}

    def __len__(self):
        return len(self.docs)

    def add_doc(self, doc):
        """Add a document to the collection."""
        self.docs[doc.get_docid()] = doc

    def remove_doc(self, doc):
        self.docs.pop(doc.get_docid())

    def update_docid(self, olddocid, newdocid):
        doc = self.docs.pop(olddocid)
        doc.set_docid(newdocid)
        self.add_doc(doc)

    def get_doc(self, docid):
        """Return a document by docid.

        Will raise a KeyError if there is no document with that ID."""
        return self.docs[docid]

    def get_docs(self):
        """Get the full list of documents.

        Returns a dictionary, with docids as keys, and docs as values."""
        return self.docs

    def inorder_iter(self):
        """Return an ordered iterator over the documents.
        
        The iterator will traverse the collection in docid order.  Modifying
        the collection while iterating over it leads to undefined results.
        Each element is a document; to find the id, call doc.get_docid()."""
        return BowCollInorderIterator(self)

    def get_num_docs(self):
        """Get the number of documents in the collection."""
        return len(self.docs)

    def get_doc_freqs(self):
        """Calculate the amount of documents each term in the collection appears in.

        Returns a dictionary, with terms as keys, and the counts as values."""
        doc_freqs = {}
        for doc in self:
            for (term, freq) in doc:
                try:
                    doc_freqs[term] += 1
                except KeyError:
                    doc_freqs[term] = 1
        return doc_freqs

    def merge_coll(self, coll):
        total_docs_self = len(self)
        for doc in coll:
            self.update_docid(doc.get_docid(), doc.get_docid() + total_docs_self)
        self.docs = dict(self.docs.items() + coll.get_docs().items())

    def __iter__(self):
        """Iterator interface.

        See inorder_iter."""
        return self.inorder_iter()

class BowCollInorderIterator:
    """Iterator over a collection."""

    def __init__(self, coll):
        """Constructor.
        
        Takes the collection we're going to iterator over as sole argument."""
        self.coll = coll
        self.keys = sorted(coll.get_docs().keys())
        self.i = 0

    def __iter__(self):
        """Iterator interface."""
        return self

    def next(self):
        """Get next element."""
        if self.i >= len(self.keys):
            raise StopIteration
        doc = self.coll.get_doc(self.keys[self.i])
        self.i += 1
        return doc

def build_coll(data, doc_class):
    coll_ = BowColl()
    docnum = 0
    for (doc, doc_parsed_content, image_url) in data:
        new_doc = doc_class(docnum, doc, image_url)
        docnum += 1
        for comment in doc_parsed_content:
            terms = re.findall(r'\w+', comment)
            terms = [term.lower() for term in terms
                        if term.lower() not in sw.stop_words
                        and len(term) > 1]
            new_doc.add_terms(terms)
        coll_.add_doc(new_doc)
    return coll_

if __name__ == '__main__':

    import sys

    if len(sys.argv) != 2:
        sys.stderr.write("USAGE: %s <wiki-coll-file>\n" % sys.argv[0])
        sys.exit()
    coll = parse_bow_coll(sys.argv[1])

    for doc in coll:
        print "%s" % doc.get_docid()
        for (term, freq) in doc:
            print "%s:%d" % (term, freq) 
        print "\n"
