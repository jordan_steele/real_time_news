import json

def output_scores(scores, fname):
    json_output = []
    for (doc, score) in scores.iteritems():
        json_output += [{
            "doc": doc.get_json(),
            "image_url": doc.get_image_url(),
            "score": score
        }]

    with open(fname, 'w') as outfile:
        json.dump(json_output, outfile, indent=2, separators=(',', ': '))
