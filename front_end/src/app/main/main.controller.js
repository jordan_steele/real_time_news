'use strict';

angular.module('realTimeNews')
  .controller('MainCtrl', function ($scope, NewsItems) {

    $scope.newsItemsLoaded = false;
    $scope.newsItems = NewsItems.query(function() {
      $scope.newsItemsLoaded = true;
    });

  });
