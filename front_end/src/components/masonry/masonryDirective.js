'use strict'

angular.module('masonryDirective', [])
  .directive('masonry', function($window, $timeout, debounce) {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      link: function(scope, element, attrs, $transclude) {
        var totalWidth, totalHeight, mason;

        var calcImgPosition = function(parentHeight, parentWidth, imgHeight, imgWidth) {
          var parentAspectRatio = parentWidth / parentHeight;
          var imgAspectRatio = imgWidth / imgHeight;

          if (imgWidth < parentWidth) {
            imgWidth = parentWidth;
            imgHeight = imgWidth / imgAspectRatio;
          }
          if (imgHeight < parentHeight) {
            imgHeight = parentHeight;
            imgWidth = imgHeight * imgAspectRatio;
          }
          if (imgHeight > parentHeight && imgWidth > parentWidth) {
            if (parentAspectRatio > imgAspectRatio) {
              imgWidth = parentWidth;
              imgHeight = imgWidth / imgAspectRatio;
            }
            if (parentAspectRatio <= imgAspectRatio) {
              imgHeight = parentHeight;
              imgWidth = imgHeight * imgAspectRatio;
            }
          }
          return {
            left: (parentWidth - imgWidth) / 2,
            top: (parentHeight - imgHeight) / 2,
            height: imgHeight,
            width: imgWidth
          };
        }

        var positionImg = function(img, left, top, width, height) {
          angular.element(img).css({
            left: left + "px",
            top: top + "px",
            height: height + "px",
            width: width + "px",
            position: "absolute"
          });
        }

        var updateImgPositions = function(imgs) {
          Array.prototype.forEach.call( imgs, function( img ) {
            var imgContainerEl = angular.element(img).parent()[0];
            var imgPosition = calcImgPosition(imgContainerEl.clientHeight,
                                              imgContainerEl.clientWidth,
                                              img.height,
                                              img.width);
            positionImg(img,
                        imgPosition.left,
                        imgPosition.top,
                        imgPosition.width,
                        imgPosition.height);
          });
        }

        var applyMason = function(imgs) {
          scope.$apply('showGrid = true');
          totalWidth = element[0].clientWidth;
          totalHeight = element[0].clientHeight;
          var ratio = totalWidth / totalHeight;
          mason = element.mason({
            itemSelector: '.box',
            ratio: ratio,
            columns: [
              [0,480,12]
            ],
            filler: {
                itemSelector: '.filler',
                filler_class: 'mason-filler'
            },
            promoted: [
              ['one-one', 1, 1],
              ['two-one', 2, 1],
              ['one-two', 1, 2],
              ['two-two', 2, 2]
            ],
            gutter: 2,
            layout: 'fluid'
          },
            function() {
              updateImgPositions(element.find('img'));
            }
          );
        }

        var destroyMason = function() {
          Array.prototype.forEach.call(element.children(), function( node ) {
            if (angular.element(node).hasClass('filler')) {
              node.parentNode.removeChild( node );
            }
          });
          mason.destroy();
        }

        var updateMason = function() {
          destroyMason();
          applyMason();
        }

        scope.showGrid = false;

        $timeout(function() {
          var imgs = element.find('img');
          var totalImages = imgs.length;
          var imgsLoaded = 0;
          imgs.bind('load' , function(e) {
            imgsLoaded += 1;

            // Once all images have loaded
            if (imgsLoaded == totalImages) {
              // Do mason
              applyMason();

              // Set up a debounced redo of mason when window is resized
              var debouncedUpdateMason = debounce(updateMason, 300);
              $window.addEventListener('resize', function() {
                scope.$apply('showGrid = false');
                debouncedUpdateMason();
              }, false);
            }

          });
        });
        
      },
      template: '<div class="grid" ng-show="showGrid" ng-transclude></div>'
    }
  });