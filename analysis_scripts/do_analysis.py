import datetime
import time

import get_twitter_trends as gtt
import get_reddit_submissions as grs
import topic_analysis as ta
import output_json as oj

def get_subs_for_doc(rapi, doc):
    subs_coll = grs.parse_subs(rapi.search(doc.get_title(), period="week"))
    if len(subs_coll) > 1:
        (dict_, lsi, corpus_lsi, index) = ta.create_topic_model(subs_coll, len(subs_coll))
        results = ta.qry_topic_model(doc, dict_, lsi, index)
        for (docnum, score) in results:
            if score < 0.7:
                subs_coll.remove_doc(subs_coll.get_doc(docnum))
            else:
                break
        return subs_coll
    else:
        return []

def get_tweets_for_doc(tapi, doc):
    return tapi.search(doc.get_title()[0:100], count=100)

def calc_reddit_score(subs):
    total_score = 0
    max_posted_ago = datetime.timedelta.min
    for sub in subs:
        posted_ago = datetime.datetime.utcfromtimestamp(time.time()) - \
                            datetime.datetime.utcfromtimestamp(sub.get_created())
        max_posted_ago = posted_ago if posted_ago > max_posted_ago else max_posted_ago
        total_score += sub.get_base_obj().score

    return total_score / max_posted_ago.total_seconds()

def calc_twitter_score(tweets):
    if len(tweets) == 0:
        return 0
    else:
        last_tweet = tweets[-1]
        posted_ago = datetime.datetime.utcfromtimestamp(time.time()) - last_tweet.created_at
        return len(tweets) / posted_ago.total_seconds()

def gen_medium_scores(rapi, tapi, whole_coll):
    scores = {}
    total_reddit_score = 0
    total_twitter_score = 0
    for doc in whole_coll:
        reddit_score = calc_reddit_score(get_subs_for_doc(rapi, doc))
        twitter_score = calc_twitter_score(get_tweets_for_doc(tapi, doc))
        scores[doc] = (reddit_score, twitter_score)

        total_reddit_score += reddit_score
        total_twitter_score += twitter_score

    return (scores, total_reddit_score, total_twitter_score)

def normalise_medium_scores(scores, total_reddit_score, total_twitter_score):
    for (doc, (reddit_score, twitter_score)) in scores.iteritems():
        scores[doc] = (reddit_score / total_reddit_score,
                        twitter_score / total_twitter_score)
    return scores

def weighted_score(reddit_score, twitter_score):
    return 0.5 * reddit_score + 0.5 * twitter_score

def gen_final_scores(scores):
    for (doc, (reddit_score, twitter_score)) in scores.iteritems():
        scores[doc] = weighted_score(reddit_score, twitter_score)
    return scores

def main():
    tapi = gtt.create_twitter_conn()
    twitter_coll = gtt.parse_trends(tapi.trends_place(1), tapi)
    print "got twitter"

    rapi = grs.create_reddit_conn()
    reddit_coll = grs.parse_subs(rapi.get_top(limit=25))
    print "got reddit"

    twitter_coll.merge_coll(reddit_coll)
    whole_coll = twitter_coll
    print "merged colls"

    (scores, total_reddit_score, total_twitter_score) = gen_medium_scores(rapi, tapi, whole_coll)
    print "got init scores"
    scores = normalise_medium_scores(scores, total_reddit_score, total_twitter_score)
    scores = gen_final_scores(scores)
    print "got final scores"

    # scores = { doc: 0.13 for doc in whole_coll }

    oj.output_scores(scores, "output.json")

if __name__ == '__main__':
    main()