'use strict';

angular.module('trustedResourceFilter', [])
  .filter('trusted', function($sce) {
    return function(url) {
      return $sce.trustAsResourceUrl(url);
    };
  });