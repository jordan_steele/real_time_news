'use strict';

angular.module('realTimeNews', [
  'ngResource',
  'ui.router',
  'debounce',
  'masonryDirective',
  'newsItemsService',
  'trustedResourceFilter'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      });

    $urlRouterProvider.otherwise('/');
  })
;
