import tweepy
import coll

consumer_key = "kcM2QObIwzBlNnOmNb7C9tmSW"
consumer_secret = "BEFeEfZjVj3uZpprod0jtLA2bfb372ESJxP3myLGr5rTy8shUX"
access_token = "182272636-nymLGMKrXJ6WJt60v4oZ3jZWs9l9xAaTUSBKAwVB"
access_token_secret = "1n8lTetuQLOpurRHpEBfsKhwW4LdtiAIZYN89l5zMqx0q"

def remove_hash_split_words(string):
    if string[0] == '#':
        start_loop_at = 2
    else:
        start_loop_at = 1

    new_string = "" + string[start_loop_at-1]
    prev_char = 'A'
    for char in string[start_loop_at:]:
        if char.isupper():
            if prev_char.isupper() or prev_char == ' ':
                new_string += char
            else:
                new_string += " " + char
        else:
            new_string += char
        prev_char = char

    return new_string

def create_twitter_conn():
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    return tweepy.API(auth)

def parse_tweets(tweets):
    return [ remove_hash_split_words(tweet.text.encode('ascii', 'ignore')) 
            for tweet in tweets ]

def get_image_url(tweets):
    for tweet in tweets:
        if hasattr(tweet, 'media'):
        # if tweet.media and tweet.media.media_url_https:
            return tweet.media.media_url_https
        # print tweet

def parse_trends(trends, api):
    parsed_trends = []
    for trend in trends[0]['trends']:
        tweets = api.search(trend['name'])
        parsed_trends += [(trend, parse_tweets(tweets), get_image_url(tweets))]

    return coll.build_coll(parsed_trends, coll.TwitterTrend)

if __name__ == '__main__':
    api = create_twitter_conn()
    print parse_trends(api.trends_place(1), api)
