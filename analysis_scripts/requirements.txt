Jinja2==2.7.2
MarkupSafe==0.21
Pillow==2.5.1
PyYAML==3.11
Pygments==1.6
Sphinx==1.2.2
Twisted==13.2.0
altgraph==0.10.2
bdist-mpkg==0.5.0
beautifulsoup4==4.3.2
bonjour-py==0.3
cssselect==0.9.1
feedfinder2==0.0.1
feedparser==5.1.3
gensim==0.10.3
ipython==2.0.0-dev
jieba==0.35
lxml==3.3.5
macholib==1.5.1
matplotlib==1.4.x
modulegraph==0.10.4
newspaper==0.0.9.6
nltk==2.0.4
nose==1.3.1
numpy==1.8.0rc1
oauthlib==0.7.2
pandas==0.13.1-213-gc174c3d
patsy==0.2.1
pika==0.9.13
praw==2.1.20
py2app==0.7.3
pyOpenSSL==0.13.1
pymc==2.3.2
pyobjc-core==2.5.1
pyobjc-framework-Accounts==2.5.1
pyobjc-framework-AddressBook==2.5.1
pyobjc-framework-AppleScriptKit==2.5.1
pyobjc-framework-AppleScriptObjC==2.5.1
pyobjc-framework-Automator==2.5.1
pyobjc-framework-CFNetwork==2.5.1
pyobjc-framework-Cocoa==2.5.1
pyobjc-framework-Collaboration==2.5.1
pyobjc-framework-CoreData==2.5.1
pyobjc-framework-CoreLocation==2.5.1
pyobjc-framework-CoreText==2.5.1
pyobjc-framework-DictionaryServices==2.5.1
pyobjc-framework-EventKit==2.5.1
pyobjc-framework-ExceptionHandling==2.5.1
pyobjc-framework-FSEvents==2.5.1
pyobjc-framework-InputMethodKit==2.5.1
pyobjc-framework-InstallerPlugins==2.5.1
pyobjc-framework-InstantMessage==2.5.1
pyobjc-framework-LatentSemanticMapping==2.5.1
pyobjc-framework-LaunchServices==2.5.1
pyobjc-framework-Message==2.5.1
pyobjc-framework-OpenDirectory==2.5.1
pyobjc-framework-PreferencePanes==2.5.1
pyobjc-framework-PubSub==2.5.1
pyobjc-framework-QTKit==2.5.1
pyobjc-framework-Quartz==2.5.1
pyobjc-framework-ScreenSaver==2.5.1
pyobjc-framework-ScriptingBridge==2.5.1
pyobjc-framework-SearchKit==2.5.1
pyobjc-framework-ServiceManagement==2.5.1
pyobjc-framework-Social==2.5.1
pyobjc-framework-SyncServices==2.5.1
pyobjc-framework-SystemConfiguration==2.5.1
pyobjc-framework-WebKit==2.5.1
pyparsing==1.5.7
pyserial==2.7
python-dateutil==2.4.0
pytz==2013.7
pyzmq==14.1.1
readline==6.2.4.1
requests==2.3.0
requests-oauthlib==0.4.1
scikit-learn==0.15-git
scipy==0.13.0b1
selenium==2.44.0
six==1.7.3
splinter==0.7.0
sqlite3dbm==0.1.4
statsmodels==0.6.0
tldextract==1.5.1
tornado==3.1.1
tweepy==3.2.0
update-checker==0.11
vboxapi==1.0
virtualenv==12.0.7
wsgiref==0.1.2
xattr==0.6.4
zope.interface==4.1.1
